# Text-manipulation

I wanted to show here that even if it's just a small script/logic, you can write a Clean, re-usable, extensible, testable code in Python, respecting the Dependency Injection Pattern and Clean Architecture

See more: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
