from dataclasses import dataclass


@dataclass
class Employee:
    """Represent an employee
    Is used to pass structured data between Providers & Use-cases
    """

    name: str
    role: str
    revenue_type: str
    earnings: int

    def __eq__(self, other):
        return (
            self.name == other.name
            and self.role == other.role
            and self.revenue_type == other.revenue_type
            and self.earnings == other.earnings
        )
