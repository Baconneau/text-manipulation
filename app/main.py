#!/usr/bin/env python

import typer
from uses_cases.average_earning_by_role import get_average_earning_by_role_use_case
from providers import FileDB

# Initialize the CLI
app = typer.Typer()

# Add this command to the CLI
@app.command()
def get_average_earning_by_role(role: str, file: str):
    """Parse arguments, initialize dependencies, and call the use-case"""
    fr = FileDB(file)
    print(get_average_earning_by_role_use_case(fr, role))


if __name__ == "__main__":
    # run the CLI
    app()
