from abc import ABC
from typing import Iterator

from entities import Employee


class DBInterface(ABC):
    """Abstract class/Interface for the DB
    We want to respect the Dependency Inject Pattern (DIP)
    """

    def get_employees_by_role(self, role: str) -> Iterator[Employee]:
        pass


class FileDB(DBInterface):
    """Implement DB from file"""

    def __init__(self, filename: str) -> None:
        self.filename = filename

    def _read(self):
        with open(self.filename, "r") as f:
            for line in f:
                yield line

    def get_employees_by_role(self, role: str) -> Iterator[Employee]:
        for line in self._read():
            data = line.split()
            if data[1] == role:
                yield Employee(
                    name=data[0],
                    role=data[1],
                    revenue_type=data[2],
                    earnings=int(data[3]),
                )
