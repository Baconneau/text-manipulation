from app.providers import FileDB
from unittest.mock import Mock

from app.entities import Employee


class TestFileDB:
    """Test the FileDB"""

    def test_get_employee_by_role(self):
        """Test that the FileDB search and construct of Employees works."""
        file_db = FileDB("no_file")
        file_db._read = Mock(
            return_value=[
                "user1 manager account 10",
                "user2 clerk sales 20",
                "user3 peon purchases 30",
                "user4 peon sales 20",
            ]
        )

        db_employees = [e for e in file_db.get_employees_by_role("clerk")]
        expected_employees = [
            Employee(name="user2", role="clerk", revenue_type="sales", earnings=20)
        ]
        assert db_employees == expected_employees
