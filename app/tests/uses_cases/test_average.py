from app import main
from app.entities import Employee
from app.providers import DBInterface
from app.uses_cases import get_average_earning_by_role_use_case


class TestGetAverageEarningByRoleUseCase:
    """Integration testing of our usecase"""

    class MockDB(DBInterface):
        """A Mock DB that implement the DBInterface and store employees in memory"""

        employees = [
            Employee("user1", "manager", "account", 10),
            Employee("user2", "manager", "sales", 20),
            Employee("user3", "manager", "sales", 60),
            Employee("user4", "clerk", "purchases", 40),
            Employee("user5", "peon", "sales", 40),
            Employee("user6", "peon", "purchases", 50),
        ]

        def get_employees_by_role(self, role):
            return [e for e in self.employees if e.role == role]

    def test_get_average_earning_by_role_use_case(self):
        """Test that the UC returns the right earnings"""
        mock_db = self.MockDB()
        assert get_average_earning_by_role_use_case(mock_db, "manager") == 30
        assert get_average_earning_by_role_use_case(mock_db, "clerk") == 40
        assert get_average_earning_by_role_use_case(mock_db, "peon") == 45
