from providers import DBInterface


def get_average_earning_by_role_use_case(db: DBInterface, role) -> int:
    """Calculate the average revenue for a specific role"""
    total, count = 0, 0
    employees = db.get_employees_by_role(role)

    for employee in employees:
        total += employee.earnings
        count += 1
    return int(total / count) if count else 0
