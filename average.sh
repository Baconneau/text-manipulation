#!/bin/bash

average () {
    file=$2
    res=0
    count=0
    
    while read line; do
        res=$(($res + $line))
        count=$((count+=1))
    done < <(awk -v search="$1" '$0 ~ search {print $4}' $file)

    if [[ $count == 0 ]]
    then
        echo 0
    else
        echo $(($res/$count))
    fi
}

average $1 $2
